import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var startButton: UIButton!
    @IBAction func startAction(_ sender: Any) { }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startButton.layer.cornerRadius = 8
    }
}


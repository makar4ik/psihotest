import UIKit

class TestVC: UIViewController {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var questionNumber: UILabel!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var doesntKnowButton: UIButton!
    
    private var psychoModel = PsychoModel()
    private var isUserFinishTest = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationView()
    }
    
    private func configurationView() {
        progressView.progress = 1/36
        yesButton.layer.cornerRadius = 8
        noButton.layer.cornerRadius = 8
        doesntKnowButton.layer.cornerRadius = 8
    }
    
    @IBAction func yesAction(_ sender: Any) {
        checkResult ()
        psychoModel.yesCount+=1
    }
    
    @IBAction func noAction(_ sender: Any) {
        checkResult ()
        psychoModel.noCount+=1
    }
    
    @IBAction func doesntKnowAction(_ sender: Any) {
        if isUserFinishTest {
            self.dismiss(animated: true, completion: nil)
        } else {
            checkResult ()
            psychoModel.doesntKnowCount+=1
        }
    }
    
    func checkResult () {
        progressView.progress+=1/36
        questionNumber.text = psychoModel.questionText
        if psychoModel.questionIndex == psychoModel.questions.count-1 {
            isUserFinishTest = true
            calcResult()
            return
        }
        question.text = psychoModel.questionNamber
        psychoModel.questionIndex+=1
    }
    
    func calcResult() {
        question.text = psychoModel.solution().rawValue
        progressView.isHidden = true 
        questionNumber.isHidden = true
        yesButton.isHidden = true
        noButton.isHidden = true
        doesntKnowButton.setTitle("Готово", for: .normal)
    }
}
